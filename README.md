# SMAC UI CSS


## Description
A CSS file that closely replicates the look and feel of the UI in the video game Sid Meier's Alpha Centauri. I created it as part of of my [Centauri Anthology](https://www.centaurianthology.org/) fan project and my own personal website.

## Roadmap
I don't currently plan on making any changes right now.

## License
MIT License, please use it!

## Project status
I don't have any current plans to work on this for now, but I may add some HTML examples later to demonstrate how it works.